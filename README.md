TechTestApp
==================================

## Sample app implemented to demonstrate Android skills.

### Requirements
- Min SDK 22
- Target SDK 28

### Running
- Can be built from command line using 
> ./gradlew assembleDebug
- Tests can be run via 
>./gradlew test

### Short description
- Kotlin
- MVVM architecture
- Nav Graph for navigation
- Room for storage
- Retrofit for API calls
- Dagger 2 for dependency injection inspired by https://android.jlelse.eu/android-mvvm-with-dagger-2-retrofit-rxjava-architecture-components-6f5da1a75135