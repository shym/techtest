package pl.jsm.techtest.data.repository

import io.reactivex.Single
import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.data.toEntity
import pl.jsm.techtest.db.dao.CommentDao
import pl.jsm.techtest.net.TypicodeService
import javax.inject.Inject

class CommentsRepository @Inject constructor(
    private val service: TypicodeService,
    private val commentDao: CommentDao
) {

    fun getCommentsForPost(postId: Int): Single<List<Comment>> {
        return service.getCommentsForPost(postId)
            .flattenAsObservable { it }
            .map { it.toEntity() }
            .toList()
            .doOnSuccess { commentDao.insertAll(it) }
            .onErrorResumeNext { commentDao.getAll(postId) }
    }

}