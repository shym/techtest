package pl.jsm.techtest.data

import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.db.User
import pl.jsm.techtest.net.CommentDto
import pl.jsm.techtest.net.PostDto
import pl.jsm.techtest.net.UserDto

fun PostDto.toEntity(): Post = Post(id, userId, title, body)

fun UserDto.toEntity(): User = User(id, name, email)

fun CommentDto.toEntity(): Comment = Comment(id, postId, name, email, body)