package pl.jsm.techtest.data.repository

import io.reactivex.Single
import pl.jsm.techtest.db.User
import pl.jsm.techtest.data.toEntity
import pl.jsm.techtest.db.dao.UserDao
import pl.jsm.techtest.net.TypicodeService
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val service: TypicodeService,
    private val userDao: UserDao
) {

    fun getUserForId(id: Int): Single<User> {
        return service.getUser(id)
            .flattenAsObservable { it }
            .singleOrError()
            .map { it.toEntity() }
            .doOnSuccess { userDao.insert(it) }
            .onErrorResumeNext { userDao.getUser(id) }
    }
}