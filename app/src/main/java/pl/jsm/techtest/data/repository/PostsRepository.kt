package pl.jsm.techtest.data.repository

import io.reactivex.Single
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.data.toEntity
import pl.jsm.techtest.db.dao.PostDao
import pl.jsm.techtest.net.TypicodeService
import javax.inject.Inject

class PostsRepository @Inject constructor(
    private val service: TypicodeService,
    private val postDao: PostDao
) {

    fun getPosts(): Single<List<Post>> {
        return service.getPosts()
            .flattenAsObservable { it }
            .map { it.toEntity() }
            .toList()
            .doOnSuccess { postDao.insertAll(it) }
            .onErrorResumeNext { postDao.getAll() }
    }

}