package pl.jsm.techtest.ui.main.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.transition.TransitionInflater
import com.github.nitrico.lastadapter.LastAdapter
import com.google.android.material.snackbar.Snackbar
import pl.jsm.techtest.BR
import pl.jsm.techtest.R
import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.databinding.FragmentPostDetailsBinding
import pl.jsm.techtest.ui.base.BaseDaggerFragment
import javax.inject.Inject

class PostDetailsFragment : BaseDaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentPostDetailsBinding
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(PostDetailsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_details, container, false)
        binding.model = viewModel
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeToNavigationEvents()
        setupToolbar(binding.toolbar)
        initializeList()
    }

    private fun subscribeToNavigationEvents() {
        viewModel.showFetchingError.observe(this, Observer { showIoError() })
    }

    private fun initializeList() {
        LastAdapter(viewModel.comments, BR.comment)
            .map(Comment::class.java, R.layout.row_comment)
            .into(binding.commentsList)

        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.refreshComments() }
    }

    private fun showIoError() {
        Snackbar.make(binding.root, R.string.error_fetching_data, Snackbar.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getArgs()
    }

    private fun getArgs() {
        val post = arguments?.let { PostDetailsFragmentArgs.fromBundle(it).post }
        post?.let { viewModel.updatePost(it) }
    }

    override fun getFragmentTitle(): Int = R.string.post_details_title

}