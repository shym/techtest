package pl.jsm.techtest.ui.main.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.github.nitrico.lastadapter.LastAdapter
import com.github.nitrico.lastadapter.Type
import com.google.android.material.snackbar.Snackbar
import pl.jsm.techtest.BR
import pl.jsm.techtest.R
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.databinding.FragmentPostsBinding
import pl.jsm.techtest.databinding.RowPostBinding
import pl.jsm.techtest.ui.base.BaseDaggerFragment
import pl.jsm.techtest.ui.view.getViewForItemAtPosition
import javax.inject.Inject

class PostsFragment : BaseDaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentPostsBinding
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(PostsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_posts, container, false)
        binding.model = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscribeToNavigationEvents()
        initializeList()
        viewModel.fetchPosts()
        setupToolbar(binding.toolbar)
    }

    private fun subscribeToNavigationEvents() {
        viewModel.navigateToPostDetails.observe(this, Observer { goToPostDetailsAnimated(it) })
        viewModel.showFetchingError.observe(this, Observer { showIoError() })
    }

    private fun initializeList() {
        val postType = Type<RowPostBinding>(R.layout.row_post)
            .onClick { it.binding.post?.let { post -> viewModel.postClicked(post) } }
        LastAdapter(viewModel.posts, BR.post)
            .map<Post>(postType)
            .into(binding.recyclerView)

        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.fetchPosts(true) }
    }

    private fun goToPostDetailsAnimated(post: Post) {
        val position = viewModel.posts.indexOf(post)
        val recycler = binding.recyclerView

        recycler.getViewForItemAtPosition(position)?.let {
            val rowBinding = DataBindingUtil.findBinding<RowPostBinding>(it)
            rowBinding?.let {
                val card = rowBinding.card
                val transitionCard = "card"
                val extras = FragmentNavigatorExtras(
                    card to transitionCard
                )
                findNavController().navigate(PostsFragmentDirections.startPostDetails(post), extras)
            }
        }
    }

    private fun showIoError() {
        Snackbar.make(binding.root, R.string.error_fetching_data, Snackbar.LENGTH_SHORT).show()
    }

    override fun getFragmentTitle(): Int = R.string.posts_title
}
