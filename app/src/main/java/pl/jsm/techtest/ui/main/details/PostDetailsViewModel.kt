package pl.jsm.techtest.ui.main.details

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.db.User
import pl.jsm.techtest.data.repository.CommentsRepository
import pl.jsm.techtest.data.repository.UsersRepository
import pl.jsm.techtest.ui.base.BaseViewModel
import pl.jsm.techtest.ui.base.SingleLiveEvent
import javax.inject.Inject

class PostDetailsViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
    private val commentsRepository: CommentsRepository
) : BaseViewModel() {

    val postTile = ObservableField<String>()
    val postBody = ObservableField<String>()
    val userEmail = ObservableField<String>()
    val userName = ObservableField<String>()
    val comments = ObservableArrayList<Comment>()
    val isLoadingComments = ObservableBoolean()
    val showFetchingError = SingleLiveEvent<Void>()
    private var post: Post? = null

    fun updatePost(post: Post) {
        if (this.post == post) return
        this.post = post
        postTile.set(post.title)
        postBody.set(post.body)
        fetchPostAuthor(post.userId)
        fetchComments(post.id)
    }

    fun refreshComments() {
        post?.let { fetchComments(it.id) }
    }

    private fun fetchPostAuthor(userId: Int) {
        registerDisposable(
            usersRepository.getUserForId(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handlePostAuthor) { handleAuthorFetchingError() }
        )
    }

    private fun handlePostAuthor(user: User) {
        userEmail.set(user.email)
        userName.set(user.name)
    }

    private fun handleAuthorFetchingError() {
        showFetchingError.call()
    }

    private fun fetchComments(postId: Int) {
        registerDisposable(
            commentsRepository.getCommentsForPost(postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { isLoadingComments.set(true) }
                .subscribe(this::handleComments) { handleCommentsFetchError() }
        )
    }

    private fun handleComments(newComments: List<Comment>) {
        if (newComments != comments) {
            comments.apply {
                clear()
                addAll(newComments)
            }
        }
        isLoadingComments.set(false)
    }

    private fun handleCommentsFetchError() {
        isLoadingComments.set(false)
        showFetchingError.call()
    }
}