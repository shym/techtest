package pl.jsm.techtest.ui.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.getViewForItemAtPosition(position: Int): View? {
    return findViewHolderForAdapterPosition(position)?.itemView
}