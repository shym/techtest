package pl.jsm.techtest.ui.main

import pl.jsm.techtest.ui.base.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : BaseViewModel()