package pl.jsm.techtest.ui.base

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import pl.jsm.techtest.BuildConfig
import pl.jsm.techtest.ui.view.GlideApp

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("isRefreshing")
    fun isRefreshing(view: SwipeRefreshLayout, refreshing: Boolean) {
        view.isRefreshing = refreshing
    }

    @JvmStatic
    @BindingAdapter("userAvatar")
    fun loadUserAvatar(view: ImageView, email: String?) {
        email?.let {
            GlideApp.with(view).clear(view)
            GlideApp.with(view)
                .load(BuildConfig.AVATAR_SOURCE + it)
                .circleCrop()
                .into(view)
        }
    }

    @JvmStatic
    @BindingAdapter("visibleIf")
    fun visibleIf(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }
}