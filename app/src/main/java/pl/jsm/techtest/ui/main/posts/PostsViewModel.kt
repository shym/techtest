package pl.jsm.techtest.ui.main.posts

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import pl.jsm.techtest.data.repository.PostsRepository
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.ui.base.BaseViewModel
import pl.jsm.techtest.ui.base.SingleLiveEvent
import javax.inject.Inject

class PostsViewModel @Inject constructor(private val postsRepository: PostsRepository) : BaseViewModel() {

    val navigateToPostDetails = SingleLiveEvent<Post>()
    val showFetchingError = SingleLiveEvent<Void>()
    val posts = ObservableArrayList<Post>()
    val isLoadingData = ObservableBoolean()

    fun postClicked(post: Post) {
        navigateToPostDetails.value = post
    }

    fun fetchPosts(force: Boolean = false) {
        if (posts.isEmpty() || force) {
            registerDisposable(
                postsRepository.getPosts()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { isLoadingData.set(true) }
                    .subscribe(this::handlePosts) { handleError() }
            )
        }
    }

    private fun handlePosts(newPosts: List<Post>) {
        if (newPosts != posts) {
            posts.clear()
            posts.addAll(newPosts)
        }
        isLoadingData.set(false)
    }

    private fun handleError() {
        isLoadingData.set(false)
        showFetchingError.call()
    }
}