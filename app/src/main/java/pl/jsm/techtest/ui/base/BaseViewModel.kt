package pl.jsm.techtest.ui.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val disposables = CompositeDisposable()

    protected fun registerDisposable(d: Disposable) {
        disposables.add(d)
    }

    private fun clearDisposables() {
        disposables.clear()
    }

    override fun onCleared() {
        clearDisposables()
        super.onCleared()
    }
}