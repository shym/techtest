package pl.jsm.techtest.ui.view

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class TechTestGlideModule : AppGlideModule()