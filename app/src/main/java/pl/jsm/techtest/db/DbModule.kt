package pl.jsm.techtest.db

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import pl.jsm.techtest.db.dao.CommentDao
import pl.jsm.techtest.db.dao.PostDao
import pl.jsm.techtest.db.dao.UserDao
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application.applicationContext,
            AppDatabase::class.java, "dbName"
        ).build()
    }

    @Provides
    @Singleton
    fun providePostDao(appDatabase: AppDatabase): PostDao = appDatabase.postDao()

    @Provides
    @Singleton
    fun provideCommentDao(appDatabase: AppDatabase): CommentDao = appDatabase.commentDao()

    @Provides
    @Singleton
    fun provideUserDao(appDatabase: AppDatabase): UserDao = appDatabase.userDao()

}