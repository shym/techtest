package pl.jsm.techtest.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import pl.jsm.techtest.db.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUser(id: Int): Single<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<User>)

}