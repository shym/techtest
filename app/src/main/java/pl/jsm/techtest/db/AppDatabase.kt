package pl.jsm.techtest.db

import androidx.room.Database
import androidx.room.RoomDatabase
import pl.jsm.techtest.db.dao.CommentDao
import pl.jsm.techtest.db.dao.PostDao
import pl.jsm.techtest.db.dao.UserDao

@Database(entities = [Post::class, Comment::class, User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao
    abstract fun userDao(): UserDao
}