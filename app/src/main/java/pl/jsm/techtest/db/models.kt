package pl.jsm.techtest.db

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Post(
    @PrimaryKey var id: Int,
    var userId: Int,
    var title: String,
    var body: String
) : Parcelable

@Entity
data class Comment(
    @PrimaryKey var id: Int,
    var postId: Int,
    var name: String,
    var email: String,
    var body: String
)

@Entity
data class User(
    @PrimaryKey var id: Int,
    var name: String,
    var email: String
)
