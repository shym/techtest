package pl.jsm.techtest.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import pl.jsm.techtest.db.Comment

@Dao
interface CommentDao {

    @Query("SELECT * FROM comment")
    fun getAll(): Single<List<Comment>>

    @Query("SELECT * FROM comment WHERE postId = :postId")
    fun getAll(postId: Int): Single<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<Comment>)
}