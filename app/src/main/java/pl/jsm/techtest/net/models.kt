package pl.jsm.techtest.net


data class PostDto(
    var id: Int,
    var userId: Int,
    var title: String,
    var body: String
)

data class CommentDto(
    var id: Int,
    var postId: Int,
    var name: String,
    var email: String,
    var body: String
)

data class UserDto(
    var id: Int,
    var name: String,
    var email: String
)
