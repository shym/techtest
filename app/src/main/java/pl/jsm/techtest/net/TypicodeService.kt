package pl.jsm.techtest.net

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface TypicodeService {

    @GET("/posts")
    fun getPosts(): Single<List<PostDto>>

    @GET("/comments")
    fun getCommentsForPost(@Query("postId") postId: Int): Single<List<CommentDto>>

    @GET("/comments")
    fun getAllComments(): Single<List<CommentDto>>

    @GET("/users")
    fun getUser(@Query("id") userId: Int): Single<List<UserDto>>

    @GET("/users")
    fun getAllUsers(): Single<List<UserDto>>

}