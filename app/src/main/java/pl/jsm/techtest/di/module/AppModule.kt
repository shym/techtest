package pl.jsm.techtest.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.jsm.techtest.db.DbModule
import pl.jsm.techtest.di.module.vm.ViewModelModule
import pl.jsm.techtest.net.NetModule
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, NetModule::class, DbModule::class])
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application
}