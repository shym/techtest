package pl.jsm.techtest.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.jsm.techtest.ui.main.details.PostDetailsFragment
import pl.jsm.techtest.ui.main.posts.PostsFragment

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun contributePostsFragment(): PostsFragment

    @ContributesAndroidInjector
    abstract fun contributePostDetailsFragment(): PostDetailsFragment
}