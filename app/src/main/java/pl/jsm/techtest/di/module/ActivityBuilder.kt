package pl.jsm.techtest.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.jsm.techtest.ui.main.MainActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

}