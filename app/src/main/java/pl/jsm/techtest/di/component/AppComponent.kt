package pl.jsm.techtest.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import pl.jsm.techtest.TechTestApplication
import pl.jsm.techtest.di.module.ActivityBuilder
import pl.jsm.techtest.di.module.AppModule
import pl.jsm.techtest.di.module.FragmentBuilder
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivityBuilder::class, FragmentBuilder::class])
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: TechTestApplication)
}