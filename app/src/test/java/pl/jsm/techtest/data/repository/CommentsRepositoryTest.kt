package pl.jsm.techtest.data.repository

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.`when`
import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.db.dao.CommentDao
import pl.jsm.techtest.net.CommentDto
import pl.jsm.techtest.net.TypicodeService
import java.lang.Exception

class CommentsRepositoryTest {

    lateinit var repository: CommentsRepository

    private val mockedService: TypicodeService = mock()
    private val mockedDao: CommentDao = mock()
    private val dtoComment = CommentDto(1, 2, "dto", "dto@email.com", "dtoBody")
    private val transformedComment = Comment(1, 2, "dto", "dto@email.com", "dtoBody")
    private val entityComment = Comment(2, 3, "entity", "entity@e.com", "entityB")

    @Before
    fun setUp() {
        repository = CommentsRepository(mockedService, mockedDao)
    }

    @Test
    fun getCommentsForPost_apiRespondsOk_commentIsCached() {
        `when`(mockedService.getCommentsForPost(2)).thenReturn(Single.just(listOf(dtoComment)))

        val result = repository.getCommentsForPost(2).test()

        result.assertComplete()
        result.assertValue(listOf(transformedComment))
        verify(mockedService).getCommentsForPost(2)
        verify(mockedDao).insertAll(listOf(transformedComment))
    }

    @Test
    fun getCommentsForPost_apiThrowsError_commentTakenFromCache() {
        `when`(mockedService.getCommentsForPost(2)).thenReturn(Single.error(Exception("Boom")))
        `when`(mockedDao.getAll(2)).thenReturn(Single.just(listOf(entityComment)))

        val result = repository.getCommentsForPost(2).test()

        result.assertComplete()
        result.assertValue(listOf(entityComment))
        verify(mockedService).getCommentsForPost(2)
        verify(mockedDao).getAll(2)
    }

    @Test
    fun getCommentsForPost_apiThrowsError_cacheThrowsError() {
        val cacheError = Exception("cache failed")
        val apiError = Exception("api failed")
        `when`(mockedService.getCommentsForPost(2)).thenReturn(Single.error(apiError))
        `when`(mockedDao.getAll(2)).thenReturn(Single.error(cacheError))

        val result = repository.getCommentsForPost(2).test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getCommentsForPost(2)
        verify(mockedDao).getAll(2)
    }

    @Test
    fun getCommentsForPost_apiThrowsError_cacheIsEmpty() {
        val cacheError = Exception("cache failed")
        val apiError = NoSuchElementException()
        `when`(mockedService.getCommentsForPost(2)).thenReturn(Single.error(apiError))
        `when`(mockedDao.getAll(2)).thenReturn(Single.error(cacheError))

        val result = repository.getCommentsForPost(2).test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getCommentsForPost(2)
        verify(mockedDao).getAll(2)
    }
}