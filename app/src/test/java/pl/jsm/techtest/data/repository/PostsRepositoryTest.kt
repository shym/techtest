package pl.jsm.techtest.data.repository

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.`when`
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.db.dao.PostDao
import pl.jsm.techtest.net.PostDto
import pl.jsm.techtest.net.TypicodeService
import java.lang.Exception

class PostsRepositoryTest {

    lateinit var repository: PostsRepository

    private val mockedService: TypicodeService = mock()
    private val mockedDao: PostDao = mock()
    private val dtoPost = PostDto(1, 2, "dto", "dtoBody")
    private val transformedPost = Post(1, 2, "dto", "dtoBody")
    private val entityPost = Post(2, 3, "entity", "entityBody")

    @Before
    fun setUp() {
        repository = PostsRepository(mockedService, mockedDao)
    }

    @Test
    fun getPosts_apiRespondsOk_postIsCached() {
        `when`(mockedService.getPosts()).thenReturn(Single.just(listOf(dtoPost)))

        val result = repository.getPosts().test()

        result.assertComplete()
        result.assertValue(listOf(transformedPost))
        verify(mockedService).getPosts()
        verify(mockedDao).insertAll(listOf(transformedPost))
    }

    @Test
    fun getPosts_apiThrowsError_postTakenFromCache() {
        `when`(mockedService.getPosts()).thenReturn(Single.error(Exception("Boom")))
        `when`(mockedDao.getAll()).thenReturn(Single.just(listOf(entityPost)))

        val result = repository.getPosts().test()

        result.assertComplete()
        result.assertValue(listOf(entityPost))
        verify(mockedService).getPosts()
        verify(mockedDao).getAll()
    }

    @Test
    fun getPosts_apiThrowsError_cacheThrowsError() {
        val cacheError = Exception("cache failed")
        val apiError = Exception("api failed")
        `when`(mockedService.getPosts()).thenReturn(Single.error(apiError))
        `when`(mockedDao.getAll()).thenReturn(Single.error(cacheError))

        val result = repository.getPosts().test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getPosts()
        verify(mockedDao).getAll()
    }

    @Test
    fun getPosts_apiThrowsError_cacheIsEmpty() {
        val cacheError = Exception("cache failed")
        val apiError = NoSuchElementException()
        `when`(mockedService.getPosts()).thenReturn(Single.error(apiError))
        `when`(mockedDao.getAll()).thenReturn(Single.error(cacheError))

        val result = repository.getPosts().test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getPosts()
        verify(mockedDao).getAll()
    }
}