package pl.jsm.techtest.data.repository

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

import org.mockito.Mockito.`when`
import pl.jsm.techtest.db.User
import pl.jsm.techtest.db.dao.UserDao
import pl.jsm.techtest.net.TypicodeService
import pl.jsm.techtest.net.UserDto
import java.lang.Exception

class UsersRepositoryTest {

    lateinit var repository: UsersRepository

    private val mockedService: TypicodeService = mock()
    private val mockedDao: UserDao = mock()
    private val dtoUser = UserDto(1, "dto", "dto@email.com")
    private val transformedUser = User(1, "dto", "dto@email.com")
    private val entityUser = User(2, "entity", "entity@email.com")

    @Before
    fun setUp() {
        repository = UsersRepository(mockedService, mockedDao)
    }

    @Test
    fun getUserForId_apiRespondsOk_userIsCached() {
        `when`(mockedService.getUser(1)).thenReturn(Single.just(listOf(dtoUser)))

        val result = repository.getUserForId(1).test()

        result.assertComplete()
        result.assertValue(transformedUser)
        verify(mockedService).getUser(1)
        verify(mockedDao).insert(transformedUser)
    }

    @Test
    fun getUserForId_apiThrowsError_userTakenFromCache() {
        `when`(mockedService.getUser(1)).thenReturn(Single.error(Exception("Boom")))
        `when`(mockedDao.getUser(1)).thenReturn(Single.just(entityUser))

        val result = repository.getUserForId(1).test()

        result.assertComplete()
        result.assertValue(entityUser)
        verify(mockedService).getUser(1)
        verify(mockedDao).getUser(1)
    }

    @Test
    fun getUserForId_apiThrowsError_cacheThrowsError() {
        val cacheError = Exception("cache failed")
        val apiError = Exception("api failed")
        `when`(mockedService.getUser(1)).thenReturn(Single.error(apiError))
        `when`(mockedDao.getUser(1)).thenReturn(Single.error(cacheError))

        val result = repository.getUserForId(1).test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getUser(1)
        verify(mockedDao).getUser(1)
    }

    @Test
    fun getUserForId_apiThrowsError_cacheIsEmpty() {
        val apiError = Exception("api failed")
        val cacheError = NoSuchElementException()
        `when`(mockedService.getUser(1)).thenReturn(Single.error(apiError))
        `when`(mockedDao.getUser(1)).thenReturn(Single.error(cacheError))

        val result = repository.getUserForId(1).test()

        result.assertNotComplete()
        assertEquals(1, result.errorCount())
        result.assertError(cacheError)
        verify(mockedService).getUser(1)
        verify(mockedDao).getUser(1)
    }

}
