package pl.jsm.techtest.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.ClassRule
import org.junit.Rule

abstract class BaseTestCase {
    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()
}