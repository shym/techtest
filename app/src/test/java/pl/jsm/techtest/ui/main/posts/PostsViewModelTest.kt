package pl.jsm.techtest.ui.main.posts

import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import pl.jsm.techtest.base.BaseTestCase
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.data.repository.PostsRepository
import java.lang.Exception

class PostsViewModelTest : BaseTestCase() {

    lateinit var viewModel: PostsViewModel

    private val mockedPostsRepository: PostsRepository = mock()
    private val mockedPost: Post = mock()
    private val mockedPosts = listOf<Post>(mock(), mock(), mock())
    private val mockedPostsResponse = Single.just(mockedPosts)

    @Before
    fun setUp() {
        viewModel = PostsViewModel(mockedPostsRepository)
    }

    @Test
    fun postClicked_verifyNavigationFired() {
        val observer = mock<Observer<Post>>()
        viewModel.navigateToPostDetails.observeForever(observer)

        viewModel.postClicked(mockedPost)

        verify(observer).onChanged(mockedPost)
    }

    @Test
    fun fetchPosts_withoutForce_postsNotEmpty() {
        viewModel.posts.add(mock())

        viewModel.fetchPosts()

        verifyNoMoreInteractions(mockedPostsRepository)
        assertFalse(viewModel.isLoadingData.get())
    }

    @Test
    fun fetchPosts_withForce_postsNotEmpty() {
        viewModel.posts.add(mock())
        whenever(mockedPostsRepository.getPosts()).thenReturn(mockedPostsResponse)
        assertNotEquals(viewModel.posts, mockedPosts)

        viewModel.fetchPosts(true)

        assertEquals(viewModel.posts, mockedPosts)
        assertFalse(viewModel.isLoadingData.get())
    }

    @Test
    fun fetchPosts_throwsError() {
        val observer = mock<Observer<Any>>()
        viewModel.showFetchingError.observeForever(observer)
        `when`(mockedPostsRepository.getPosts()).thenReturn(Single.error(Exception("Boom")))

        viewModel.fetchPosts()

        verify(observer).onChanged(null)
        assertFalse(viewModel.isLoadingData.get())
        assertTrue(viewModel.posts.isEmpty())
    }
}