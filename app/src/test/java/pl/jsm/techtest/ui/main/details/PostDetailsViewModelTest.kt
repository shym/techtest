package pl.jsm.techtest.ui.main.details

import androidx.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.`when`
import pl.jsm.techtest.base.BaseTestCase
import pl.jsm.techtest.data.repository.CommentsRepository
import pl.jsm.techtest.data.repository.UsersRepository
import pl.jsm.techtest.db.Comment
import pl.jsm.techtest.db.Post
import pl.jsm.techtest.db.User
import java.lang.Exception

class PostDetailsViewModelTest: BaseTestCase() {

    lateinit var viewModel: PostDetailsViewModel

    private val mockedUserRepository: UsersRepository = mock()
    private val mockedCommentsRepository: CommentsRepository = mock()
    private val post = Post(1, 2, "title", "body")
    private val user = User(1, "name", "some@email.com")
    private val comments = listOf<Comment>(mock(), mock(), mock())
    private val observer = mock<Observer<Void>>()

    @Before
    fun setUp() {
        viewModel = PostDetailsViewModel(mockedUserRepository, mockedCommentsRepository)
        viewModel.showFetchingError.observeForever(observer)
    }

    @Test
    fun updatePost_happyPath() {
        assertEmptyState()
        `when`(mockedUserRepository.getUserForId(eq(post.userId))).thenReturn(Single.just(user))
        `when`(mockedCommentsRepository.getCommentsForPost(eq(post.id))).thenReturn(Single.just(comments))

        viewModel.updatePost(post)

        assertFilledOutState()
    }

    @Test
    fun updatePost_samePost_noDuplicateCalls() {
        assertEmptyState()
        `when`(mockedUserRepository.getUserForId(eq(post.userId))).thenReturn(Single.just(user))
        `when`(mockedCommentsRepository.getCommentsForPost(eq(post.id))).thenReturn(Single.just(comments))

        viewModel.updatePost(post)

        assertFilledOutState()

        viewModel.updatePost(post)
        verify(mockedUserRepository).getUserForId(eq(post.userId))
        verify(mockedCommentsRepository).getCommentsForPost(eq(post.id))
        assertFilledOutState()
    }

    @Test
    fun updatePost_couldNotFetchUser() {
        assertEmptyState()
        `when`(mockedUserRepository.getUserForId(eq(post.userId))).thenReturn(Single.error(Exception()))
        `when`(mockedCommentsRepository.getCommentsForPost(eq(post.id))).thenReturn(Single.just(comments))

        viewModel.updatePost(post)

        assertFilledOutWithoutUser()
        verify(observer, times(1)).onChanged(null)
    }

    @Test
    fun updatePost_couldNotFetchComments() {
        assertEmptyState()
        `when`(mockedUserRepository.getUserForId(eq(post.userId))).thenReturn(Single.just(user))
        `when`(mockedCommentsRepository.getCommentsForPost(eq(post.id))).thenReturn(Single.error(Exception()))

        viewModel.updatePost(post)

        assertFilledOutWithoutComments()
        verify(observer, times(1)).onChanged(null)
    }

    @Test
    fun updatePost_couldNotFetchCommentsAndUser() {
        assertEmptyState()
        `when`(mockedUserRepository.getUserForId(eq(post.userId))).thenReturn(Single.error(Exception()))
        `when`(mockedCommentsRepository.getCommentsForPost(eq(post.id))).thenReturn(Single.error(Exception()))

        viewModel.updatePost(post)

        assertFilledOutWithoutUserAndComments()
        verify(observer, times(2)).onChanged(null)
    }

    @Test
    fun refreshComments_noPost() {
        assertEmptyState()

        viewModel.refreshComments()

        verifyNoMoreInteractions(mockedCommentsRepository)
        assertEmptyState()
        verifyNoMoreInteractions(observer)
    }

    private fun assertEmptyState() {
        assertPostEmpty()
        assertUserEmpty()
        assertCommentsEmpty()
    }

    private fun assertFilledOutState() {
        assertPostFilledOut()
        assertUserFilledOut()
        assertCommentsFilledOut()
    }

    private fun assertFilledOutWithoutUser() {
        assertPostFilledOut()
        assertUserEmpty()
        assertCommentsFilledOut()
    }

    private fun assertFilledOutWithoutComments() {
        assertPostFilledOut()
        assertUserFilledOut()
        assertCommentsEmpty()
    }

    private fun assertFilledOutWithoutUserAndComments() {
        assertPostFilledOut()
        assertUserEmpty()
        assertCommentsEmpty()
    }

    private fun assertPostEmpty() {
        assertEquals(null, viewModel.postTile.get())
        assertEquals(null, viewModel.postBody.get())
    }

    private fun assertUserEmpty() {
        assertEquals(null, viewModel.userEmail.get())
        assertEquals(null, viewModel.userName.get())
    }

    private fun assertCommentsEmpty() {
        assertTrue(viewModel.comments.isEmpty())
        assertFalse(viewModel.isLoadingComments.get())
    }

    private fun assertPostFilledOut() {
        assertEquals(post.title, viewModel.postTile.get())
        assertEquals(post.body, viewModel.postBody.get())
    }

    private fun assertUserFilledOut() {
        assertEquals(user.email, viewModel.userEmail.get())
        assertEquals(user.name, viewModel.userName.get())
    }

    private fun assertCommentsFilledOut() {
        assertEquals(comments, viewModel.comments)
        assertFalse(viewModel.isLoadingComments.get())
    }
}